from dataclasses import dataclass

@dataclass(frozen=True)
class Name:
    firstName: str
    lastName: str

    def __post_init__(self) -> None:
        print("calling post init")
        if self.firstName == None or self.lastName == None:
            raise ValueError()

    def __init__(self, fn, ln):
        print("calling init")
        self.firstName = fn
        self.lastName = ln

    def get_name(self) -> str:
        return self.firstName + ' ' + self.lastName

try: 
    name : Name = Name("Krishna Mohan", None)
    print(name.get_name())
except ValueError: 
    print("Invalid Name")