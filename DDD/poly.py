from abc import ABC, abstractmethod

class ICalc(ABC):
    @abstractmethod
    def add(self, first, second) -> int:
        pass

class Calc(ICalc):
    def add(self, first, second) -> int:
        return first+second 

class Math:
    def add(self, first, second) -> int:
        return first+second 

calc: ICalc = Math()
print(calc.add(1, 2))