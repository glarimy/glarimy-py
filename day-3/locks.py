from threading import Lock, Thread, current_thread
import time
import datetime

lock = Lock()

def process():
    print(current_thread().name, " acquiring lock at ", datetime.datetime.now())    
    lock.acquire()
    print(current_thread().name, " acquired lock at ", datetime.datetime.now())
    time.sleep(5)
    lock.release()
    print(current_thread().name, " released lock at ", datetime.datetime.now())

t1 = Thread(target=process)
t2 = Thread(target=process)
t1.start()
t2.start()

