from threading import Lock, Thread, current_thread
import time
import datetime

lock = Lock()
data = []

def read():
    while(True):
        lock.acquire()
        while(len(data) > 0):
            print("Received: ", data.pop(0))
        lock.release()
        time.sleep(5)

def write():
    while(True):
        n = input("Next number: ")
        lock.acquire()
        data.append(n)
        lock.release()

Thread(target=read).start()
Thread(target=write).start()