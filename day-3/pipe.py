from threading import Lock, Thread, current_thread
import time
import datetime

input_lock = Lock()
odd_lock = Lock()
even_lock = Lock()

input = []
odds = []
evens = []

def reader():
    input_lock.acquire()
    input.append(10)
    input.append(20)
    input.append(30)
    input.append(40)
    input.append(50)
    input_lock.release()

def processor():
    input_lock.acquire()
    while(len(input) > 0):
        e = input.pop(0)
        if(e%2 == 0):
            even_lock.acquire()
            evens.append(e)
            even_lock.release()
        else:
            odd_lock.acquire()
            odds.append(e)
            odd_lock.release()
    input_lock.release()

def writer(queue, lock):
    lock.acquire()
    while(len(queue) > 0):
        print(current_thread().name, ": ", queue.pop(0))
    lock.release() 

Thread(target=reader, name="Reader").start()
Thread(target=processor, name="Processor").start()
Thread(target=writer, name="Odd Writer", args=[odds, odd_lock]).start()
Thread(target=writer, name="Even Writer", args=[evens, even_lock]).start()