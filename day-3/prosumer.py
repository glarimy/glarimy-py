from threading import Thread, current_thread, Condition, Lock
import time
queue = []
lock = Lock()
condition = Condition(lock)

def producer() -> None:
    n = 0
    status = condition.acquire() 
    print("producer lock: ",status)
    while True:
        time.sleep(1)
        if(len(queue) > 0):
            print("producer waiting")
            condition.wait()
        n += 1
        queue.append(n)
        print("produced: ", n)
        print("producer notifying")
        condition.notify()
    condition.release()

def consumer() -> None:
    status = condition.acquire()
    print("consumer lock: ",status)
    while True: 
        time.sleep(1)
        if len(queue) == 0:
            print("consumer waiting")
            condition.wait()
        print("consumed: ", queue.pop())
        print("consumer notifying")
        condition.notify()
    condition.release()

p = Thread(target = producer, name = 'producer')
c = Thread(target = consumer, name = 'consumer')
p.start()
c.start()