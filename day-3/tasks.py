import asyncio
import datetime

async def display(id, delay):
    print(id, " is waiting to display")
    await asyncio.sleep(delay)
    print(id, " is ready to display")
    return ("hello world")

async def main():
    print("submitted the tasks at ", datetime.datetime.now())    
    #promise = asyncio.gather(display(1, 5), display(2, 5), display(3, 5))
    await display(1, 5)
    await display(2, 5)
    await display(3, 5)
    print("I mind my own business") # 1000s of lines of code
    #await promise
    print("finished the tasks at ", datetime.datetime.now()) 
    #for result in promise.result():
        #print(result)

asyncio.run(main())