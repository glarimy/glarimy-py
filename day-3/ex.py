from threading import Lock, Thread, current_thread
import time
import datetime

lock = Lock()
readyReady = True
printReady = False
promptReady = False
value = None
choice = 0

def readValue():
    global printReady, readyReady, promptReady
    promptReady = False
    while True: 
        lock.acquire()
        if choice == 1:
            if readyReady == True: 
                value = input("Enter value: ")
                readyReady = False
                printReady = True
            else:
                print("Reader not available. Try later")
        promptReady = True
        lock.release()

def printValue():
    global printReady, readyReady, promptReady
    promptReady = False
    while True: 
        lock.acquire()
        if choice == 2:
            if printReady == True:
                print(value)
                printReady = False
                readReady = True
            else:
                print("Printer not available. Try later")
        lock.release()
    promptReady = True

reader = Thread(target=readValue)
printer = Thread(target=printValue)
reader.start()
printer.start()

while True: 
    lock.acquire()
    if promptReady == True: 
        choice = int(input("1) Input 2) Print: "))
    lock.release()