import time
from threading import Thread, current_thread

class Task(Thread):
    def run(self):
        counter = 0
        while counter < 3:
            print(current_thread().name, ": ", 'hello')
            time.sleep(1)
            print(current_thread().name, ": ", 'world')
            counter = counter + 1

print(current_thread().name)
t1 = Task()
t1.start()
t2 = Task()
t2.start()
print(current_thread().name, " waiting")
t1.join()
t2.join()
print(current_thread().name, " bye")
