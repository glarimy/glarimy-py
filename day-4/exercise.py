from concurrent.futures import ThreadPoolExecutor
from threading import current_thread
import time

def task():
    print(current_thread().name, " running the task")
    time.sleep(5)
    print(current_thread().name, "completed")
    return True

executor = ThreadPoolExecutor(max_workers=2)
f1 = executor.submit(task)
f2 = executor.submit(task)
f3 = executor.submit(task)
f4 = executor.submit(task)
f5 = executor.submit(task)
print(f1.result())
print(f2.result())
print(f3.result())
print(f4.result())
print(f5.result())
