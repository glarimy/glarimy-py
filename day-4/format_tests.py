import unittest
from format_name import format_name

class FormatTestCase(unittest.TestCase):
    def test_valid_name(self):
        self.assertTrue(format_name("krishna mohan") == "Krishna Mohan")

    def test_name_with_one_part(self):
        with self.assertRaises(ValueError):
            format_name("krishna")

    def test_valid_name_with_two_parts(self):
        self.assertTrue(format_name("krishna mohan") == "Krishna Mohan")

    def test_valid_name_with_three_parts(self):
        self.assertTrue(format_name("krishna mohan koyya") == "Krishna Mohan Koyya")

    def test_name_with_four_parts(self):
        with self.assertRaises(ValueError):
            format_name("mr krishna mohan koyya")
    
    def test_name_with_shorter_lastmame(self):
        with self.assertRaises(ValueError):
            format_name("krishna k")

    def test_shorter_name(self):
        with self.assertRaises(ValueError):
            format_name("k m k")
    
    def test_lenghtier_name(self):
        with self.assertRaises(ValueError):
            format_name("sreeman srinivasa chandrasekhara krishna mohan koyya")
    
    def test_name_with_special_characters(self):
        with self.assertRaises(ValueError):
            format_name("k. krishna mohan")

unittest.main()