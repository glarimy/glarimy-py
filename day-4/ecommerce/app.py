from platform.broker.service import InMemoryBrokerService
from platform.order.service import OrderService
from platform.order.domain import Order
from platform.orchestrator.service import handleSagaCommands
from platform.inventory.service import handleInventoryCommands

broker = InMemoryBrokerService()
broker.subscribe(handleSagaCommands, "saga.commands")
broker.subscribe(handleInventoryCommands, "inventory.commands")

os = OrderService()
os.create(Order(1, 10, 100, 1))
status = os.get_status(1)
print(status)