from abc import ABC, abstractmethod
from threading import Thread

class Command:
    def __init__(self, operation, payload):
        self._operation = operation
        self._payload = payload
    
    def display(self):
        print("Operation: ", self._operation, " Payload: ", self._payload)

class IBrokerService(ABC):
    @abstractmethod
    def subscribe(self, handler, topic): pass

    @abstractmethod
    def publish(self, message, topic): pass

class InMemoryBrokerService(IBrokerService):
    _handlers = {}

    def subscribe(self, handler, topic):
        InMemoryBrokerService._handlers[topic] = handler

    def publish(self, message, topic):
        handler = InMemoryBrokerService._handlers[topic]
        thread = Thread(target=handler, args=[message, topic])
        thread.start()