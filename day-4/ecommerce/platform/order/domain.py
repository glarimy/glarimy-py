class Order:
    def __init__(self, product_id, quantity, price, customer_id):
        self._order_id = None
        self._product_id = product_id
        self._quantity = quantity
        self._price = price
        self._customer_id = customer_id
        self._status = "Pending"

    def confirm():
        if self._status != "Pending":
            raise ValueError("Order is already processed")
        self._status = "Confirmed"
    
    def reject():
        if self._status != "Pending":
            raise ValueError("Order is already processed")
        self._status = "Failed"

    def print():
        print("ID: ", self._order_id, " Status: ", self._status)