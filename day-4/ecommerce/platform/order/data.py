from abc import ABC, abstractmethod
from platform.order.domain import Order

class IOrderRepo(ABC):
    @abstractmethod
    def save(self, order): pass

    @abstractmethod
    def fetch(self, order_id): pass

class InMemoryOrderRepo(IOrderRepo):
    def __init__(self):
        self._orders = {}

    def save(self, order):
        self._orders[order._order_id] = order

    def fetch(self, order_id):
        return self._orders[order_id]