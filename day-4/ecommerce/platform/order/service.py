from platform.order.domain import Order
from platform.order.data import InMemoryOrderRepo
from platform.broker.service import InMemoryBrokerService, Command

from abc import ABC, abstractmethod

class IOrderService(ABC):
    @abstractmethod
    def create(self, order): pass

    @abstractmethod
    def get_status(self, order_id): pass

class OrderService(IOrderService):
    def __init__(self):
        self._repo = InMemoryOrderRepo()
        self._broker = InMemoryBrokerService()

    def create(self, order):
        order._order_id = 1
        self._repo.save(order)
        command = Command("process_order", {"order_id": order._order_id, "product_id": order._product_id, "quantity": order._quantity, "customer_id": order._customer_id, "price": order._price})
        self._broker.publish(command, "saga.commands")

    def get_status(self, order_id):
        return self._repo.fetch(order_id)._status