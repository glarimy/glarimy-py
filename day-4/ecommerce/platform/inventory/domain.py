class Product:
    def __init__(self, product_id, product_name, price, total_quantity, available_quantity, reserved_quantity):
        self._product_id = product_id
        self._product_name = product_name
        self._price = price
        self._total_quantity = total_quantity
        self._available_quantity = available_quantity
        self._reserved_quantity = reserved_quantity
    
    def reserve(self, quantity):
        if self._available_quantity < quantity:
            raise ValueError("Insufficient quantity")
        self._available_quantity -= quantity
        self._reserved_quantity += quantity

    def release(self, quantity):
        if self._reserved_quantity < quantity:
            raise ValueError("Inconsistent quantity")
        self._available_quantity += quantity
        self._reserved_quantity -= quantity