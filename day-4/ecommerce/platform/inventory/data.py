from abc import ABC, abstractmethod
from platform.inventory.domain import Product

class IProductRepo(ABC):
    @abstractmethod
    def fetch(self, product_id): pass

class InMemoryProductRepo(IProductRepo):
    def __init__(self):
        self.products = {}
        self.products[1] = Product(1, "Phone", 10, 10, 0)
        self.products[2] = Product(2, "Laptop", 10, 10, 0)

    def fetch(self, product_id):
        return self.products[product_id]