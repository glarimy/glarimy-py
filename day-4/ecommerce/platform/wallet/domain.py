class Wallet:
    def __init__(self, customer_id, balance):
        self._customer_id = customer_id
        self._balance = balance
    
    def debit(self, amount):
        if self._balance < amount:
            raise ValueError("Insufficient balance")
        self._balance -= amount

    def credit(self, amount):
        self._balance += amount
