from abc import ABC, abstractmethod
from domain import Wallet

class IWalletRepo(ABC):
    @abstractmethod
    def fetch(self, customer_id): pass

class InMemoryWalletRepo(IWalletRepo):
    def __init__(self):
        self.wallets = {}
        self.wallets[1] = Wallet(1, 500)
        self.wallets[2] = Wallet(2, 500)

    def fetch(self, customer_id):
        return self.wallets[customer_id]