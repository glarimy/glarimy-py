from platform.broker.service import InMemoryBrokerService, Command

broker = InMemoryBrokerService()

def handleSagaCommands(command, topic):
    command.display()
    if command._operation == "process_order":
        order = command._payload
        reserveCommand = Command("reserve_inventory", {"product_id": order["product_id"], "quantity": order["quantity"]})
        broker.publish(reserveCommand, "inventory.commands")

def handleInventoryResults(command, topic): pass

def handleWalletResults(command, topic): pass
