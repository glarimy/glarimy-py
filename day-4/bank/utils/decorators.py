from utils.broker import Broker

broker = Broker()

def MessageNotifier(*message):
    def Notifier(target):
        def capability(*args, **kwargs):
            try:
                result = target(*args, *kwargs)
                broker.notify(message[0] + " successful")
                return result
            except ValueError as e:
                broker.notify(message[0] + " failed")
                raise e
        return capability
    return Notifier