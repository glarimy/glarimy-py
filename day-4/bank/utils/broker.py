from abc import ABC, abstractmethod
from threading import Thread

class Handler(ABC):
    @abstractmethod
    def handle(self, message): pass

class Worker(Thread):
    __slots__ = ('_handler', '_message')
    def __init__(self, handler, message):
        Thread.__init__(self)
        self._handler = handler
        self._message = message 

    def run(self):
        self._handler.handle(self._message)

class Broker:
    _handlers = []

    def register(self, handler):
        Broker._handlers.append(handler)

    def notify(self, message):
        for handler in Broker._handlers:
            worker = Worker(handler, message)
            worker.start()