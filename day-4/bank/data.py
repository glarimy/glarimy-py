from domain import AccountStore

class InMemoryAccountStore(AccountStore):
    __slots__ = ('_accounts')

    def __init__(self):
        self._accounts = {}

    def save(self, account):
        self._accounts[account.number] = account
        return account

    def find(self, num):
        return self._accounts[num]