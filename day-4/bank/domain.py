from abc import ABC, abstractmethod
from utils.broker import Handler

class Name:
    def __init__(self, value):
        if(len(value.strip()) < 3):
            raise ValueError
        self._value = value

    @property
    def value(self):
        return self._value

class Pincode:
    def __init__(self, value):
        if(value < 0):
            raise ValueError

        if(len(str(value)) != 6):
            raise ValueError
        self._value = value

    @property
    def value(self):
        return self._value

class PhoneNumber:
    def __init__(self, value):
        if(value < 0):
            raise ValueError

        if(len(str(value)) != 10):
            raise ValueError
        self._value = value

    @property
    def value(self):
        return self._value

class Email:
    def __init__(self, value):
        if("@" not in value):
            raise ValueError
        self._value = value

    @property
    def value(self):
        return self._value

class Address: 
    def __init__(self, location, city, pin):
        self._location = location 
        self._city = city 
        self._pin = pin 

    @property
    def location(self):
        return self._location

    @property
    def city(self):
        return self._city

    @property
    def pin(self):
        return self._pin
    
    @property
    def value(self):
        return self._location + ", " + self._city + " - " + str(self._pin.value)

class Customer:
    __slots__ = ('_phone', 'name', 'email', 'address')
    def __init__(self, phone, name, email, address):
        if(phone == None or name == None or email == None or address == None):
            raise ValueError
        self._phone = phone
        self.name = name
        self.email = email 
        self.address = address 
    
    @property
    def phone(self):
        return self._phone

    @property
    def value(self):
        return self.name.value + ", " + str(self.phone.value) + ", " + self.email.value +", " + self.address.value

class Account:
    __slots__ = ('_num', '_balance', 'customer')
    def __init__(self, num, customer, balance=0):
        if(num < 0):
            raise ValueError
        if(customer == None):
            raise ValueError
        self._num = num 
        self._balance = balance
        self.customer = customer

    @property
    def number(self):
        return self._num
    
    @property
    def balance(self):
        return self._balance

    def credit(self, amount):
        self._balance += amount
        return self._balance

    def debit(self, amount):
        if(amount > self._balance):
            raise ValueError

        self._balance -= amount
        return self._balance

class AccountStore(ABC):
    __slots__ = ()

    @abstractmethod
    def save(self, account): pass

    @abstractmethod
    def find(self, num): pass

class EmailHandler(Handler):
    def handle(self, message):
        print("Emailer: " + message)

name = Name("Krishna")
pin = Pincode(560016)
phone = PhoneNumber(9731423166)
email = Email("krishna@glarimy.com")
address = Address("Pai Layout", "Bengaluru", pin)
krishna = Customer(phone, name, email, address)
account = Account(1, krishna, 0)
#print(str(account.number))
#print(account.credit(100))
#print(account.debit(40))
