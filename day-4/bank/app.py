from service import SimpleAccountService
from domain import Customer, Address, Name, PhoneNumber, Email, Pincode, EmailHandler
from utils.broker import Broker

class CustomerData:
    __slots__ = ('name', 'location', 'city', 'email', 'phone', 'pin')
    pass 

class AccountData:
    __slots__ = ('number', 'balance', 'customer')
    pass

class Bank: 
    def __init__(self):
        self._service = SimpleAccountService()
    
    def create(self, cust_data):
        name = Name(cust_data.name)
        pin = Pincode(cust_data.pin)
        phone = PhoneNumber(cust_data.phone)
        email = Email(cust_data.email)
        address = Address(cust_data.location, cust_data.city, pin)
        customer = Customer(phone, name, email, address)
        data = self._service.create(customer)
        account = AccountData()
        account.number = data.number
        account.balance = data.balance
        account.customer = data.customer.name.value
        return account

    def deposit(self, number, amount):
        return self._service.deposit(number, amount)

    def withdraw(self, number, amount):
        return self._service.withdraw(number, amount)

broker = Broker()
broker.register(EmailHandler())

bank = Bank()

customer = CustomerData()
customer.name = "Krishna"
customer.phone = 9731423166
customer.email = "krishna@glarimy.com"
customer.location = "Pai Layout"
customer.city = "Bengaluru"
customer.pin = 560016

account = bank.create(customer)
print("account craeted for " + account.customer)
print("account created with number: " + str(account.number))
print("account created with balance: " + str(account.balance))
print("account balance after deposit: " + str(bank.deposit(account.number, 100)))
print("account balance after withdrawl: " + str(bank.withdraw(account.number, 50)))