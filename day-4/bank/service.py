from utils.generators import get_id_generator
from data import InMemoryAccountStore
from domain import Account, Customer, Address, Name, PhoneNumber, Email, Pincode
from abc import ABC, abstractmethod
from utils.decorators import MessageNotifier

class AccountService(ABC):
    __slots__ = ()

    @abstractmethod
    def create(self, customer):pass 

    @abstractmethod
    def deposit(self, num, amount):pass 

    @abstractmethod
    def withdraw(self, num, amount):pass

class SimpleAccountService:
    __slots__ = ('_repo')
    _id_gen = get_id_generator(10000)

    def __init__(self):
        self._repo = InMemoryAccountStore()

    @MessageNotifier("account creation")
    def create(self, customer): 
        id = next(SimpleAccountService._id_gen)
        account = Account(id, customer, 0)
        self._repo.save(account)
        return account

    @MessageNotifier("account deposit")
    def deposit(self, num, amount):
        account = self._repo.find(num)
        return account.credit(amount)

    @MessageNotifier("account withdrawl")
    def withdraw(self, num, amount):
        account = self._repo.find(num)
        return account.debit(amount)

        
try: 
    name = Name("Krishna")
    pin = Pincode(560016)
    phone = PhoneNumber(9731423166)
    email = Email("krishna@glarimy.com")
    address = Address("Pai Layout", "Bengaluru", pin)
    krishna = Customer(phone, name, email, address)
    service = SimpleAccountService()
    account = service.create(krishna)
    #print(str(service.deposit(account.number, 100)))
    #print(str(service.withdraw(account.number, 50)))
    #print(str(service.withdraw(account.number, 50)))
    #print(str(service.withdraw(account.number, 50)))
except ValueError as e: 
    #print(e)
    pass

