def format_name(name):
    """
    Validates and formats the specified name based on the following conditions:
    1. Length of the name must be 8-32 characters in cluding spaces
    2. Name must have minimum two and maximum three parts
    3. Minimum length of the last part is 3
    4. Only alphabet and space are accepted
    
    Parameters: name - a string
    Returns: name in proper case
    Raises: ValueError - if any of the validations fail
    """
    pass