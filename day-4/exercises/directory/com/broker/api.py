from dataclasses import dataclass

@dataclass(frozen=True)
class Message:
    topic: str
    headers: dict
    body: str

class MessageBuilder:
    pass