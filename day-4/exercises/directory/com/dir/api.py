from abc import ABC, abstractmethod
from dataclasses import dataclass

@dataclass(frozen=True)
class User:
    name: str
    phone: int

class DirectoryError(Exception):
    def __init__(self, message):
        self.message = message

class InvalidUserError(DirectoryError):
    def __init__(self, message):
        self.message = message

class DuplicateUserError(DirectoryError):
    def __init__(self, message):
        self.message = message

class UserNotFoundError(DirectoryError):
    def __init__(self, message):
        self.message = message

class Directory(ABC):
    @abstractmethod
    def add(self, user:User) -> User:
        pass

    @abstractmethod
    def find(self, phone:int) -> User:
        pass

class Printable(Directory):
    @abstractmethod
    def print(self, phone:int):
        pass

class Factory(ABC):
    @abstractmethod
    def get(self, options:dict) -> object:
        pass