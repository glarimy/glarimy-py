from com.dir.api import Directory, User, InvalidUserError, UserNotFoundError, DuplicateUserError
from abc import ABC, abstractmethod

class Storage(ABC):
    @abstractmethod
    def create(self, user:User) -> User:
        pass
    
    @abstractmethod
    def read(self, phone:int) -> User:
        pass

    @abstractmethod
    def contains(self, phone:int) -> bool:
        pass

class InMemoryStorage(Storage):
    def __new__(self):
        if not hasattr(self, 'instance'):
            self.instance = super().__new__(self)
        return self.instance

    def __init__(self):
        self.__entries = {}

    def create(self, user:User) -> User:
        self.__entries[user.phone] = user
        return user
    
    def read(self, phone:int) -> User:
        if phone in self.__entries:
            return self.__entries[phone]
        return None

    def contains(self, phone:int) -> bool:
        return phone in self.__entries

class SimpleDirectory(Directory):
    def __init__(self, storage:Storage):
        self.__storage = storage

    def add(self, user:User) -> User:
        if self.__storage.contains(user.phone):
            raise DuplicateUserError("User already exists")
        return self.__storage.create(user)

    def find(self, phone:int) -> User:
        user:User = self.__storage.read(phone)
        if user is None:
            raise UserNotFoundError("No user found")
        return user

class ValidatableDirectory(Directory):
    def __init__(self, target:Directory):
        self.__target = target

    def add(self, user:User) -> User:
        if user is None or user.name is None or user.phone is None:
            raise InvalidUserError("User is invalid")
        return self.__target.add(user)

    def find(self, phone:int) -> User:
        return self.__target.find(phone)

class LoggableDirectory(Directory):
    def __init__(self, target:Directory):
        self.__target = target

    def add(self, user:User) -> User:
        print("Entering add")
        try:
            user:User = self.__target.add(user)
            print("Exiting add")
            return user
        except InvalidUserError as iue:
            print("Aborting add with invalid user error")
            raise iue
        except DuplicateUserError as due:
            print("Aborting add with duplicate user error")
            raise due

    def find(self, phone:int) -> User:
        print("Entering find")
        try:
            user:User = self.__target.find(phone)
            print("Exiting find")
            return user
        except UserNotFoundError as unfe:
            print("Aborting find with user not found error")
            raise unfe
    