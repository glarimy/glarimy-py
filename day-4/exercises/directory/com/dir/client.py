from com.dir.api import User, Directory, Printable, Factory, InvalidUserError
from com.dir.service import SimpleDirectory, InMemoryStorage, ValidatableDirectory, LoggableDirectory

class PrintableDirectory(Printable):
    def __init__(self, target:Directory):
        self.__target = target

    def add(self, user:User) -> User:
        return self.__target.add(user)

    def find(self, phone:int) -> User:
        return self.__target.find(phone)

    def print(self, phone:int):
        user:User = self.__target.find(phone)
        print("Name: ", user.name, "[ Phone: ", user.phone, "]")

class ObjectFactory(Factory):
    def get(self, options:dict) -> object:
        if options["object"] == "storage":
            return InMemoryStorage()

        if options["object"] == "dir":
            storage:Storage = self.get({"object": "storage"})
            dir: Directory = SimpleDirectory(storage)
            if(options["validation"] == True):
                dir = ValidatableDirectory(dir)
                if(options["logging"] == True):
                    dir = LoggableDirectory(dir)
            else:
                if(options["logging"] == True):
                    dir = LoggableDirectory(dir)
            return dir

        raise DirectoryError("Failed to boostrap")