from com.dir.client import ObjectFactory, PrintableDirectory
from com.dir.api import Factory, User, Directory, Printable, InvalidUserError, UserNotFoundError

factory : Factory = ObjectFactory()
dir:Directory = factory.get({
    "object": "dir",
    "validation": True,
    "logging": True}
)

try:
    dir.add(User("Krishna", 9731423166))
except InvalidUserError as e:
    print(e.message)

pdir: Printable = PrintableDirectory(dir)
try:
    pdir.print(9731423166)
except UserNotFoundError as e:
    print(e.message)

    