from abc import ABC, abstractmethod
from threading import Thread
from dataclasses import dataclass
import time

@dataclass
class Message():
    type: str
    body: object
    
def id_generator():
    """generates a sequential integer ID starting from 1"""
    count=1
    while True:
        yield count
        count += 1

class Notifier(Thread):
    __slots__ = ["subscriber", "message"]

    def __init__(self, subscriber, message):
        super(Notifier, self).__init__()
        self.subscriber = subscriber
        self.message = message
    
    def run(self):
        """deliver the message to the subscriber"""
        self.subscriber.on(self.message)

class Subscriber(ABC):
    @abstractmethod
    def get_type(self): 
        pass

    @abstractmethod
    def on(self, message):
        """processes the incoming message"""
        pass

class SimpleSubscriber(Subscriber):
    __slots__ = ["type"]

    def __init__(self, type):
        self.type = type

    def get_type(self):
        return self.type

    def on(self, message):
        """prints the received message on the console"""
        print(message.body)

class LazySubscriber(Subscriber):
    __slots__ = ["type"]

    def __init__(self, type):
        self.type = type

    def get_type(self):
        return self.type

    def on(self, message):
        """prints the received message on the console"""
        time.sleep(5)
        print(message.body)

class Broker():
    __slots__ = ["subscribers", "id"]

    def __init__(self):
        self.subscribers = {}
        self.id = id_generator()

    def register(self, subscriber)->int:
        """registers a subscriber with a unique id and returns the id"""
        sid = next(self.id)
        self.subscribers[sid] = subscriber
        return sid
    
    def unregister(self, id)->bool:
        """removes a subscriber with the given id"""
        del self.subscribers[id]
    
    def notify(self, message):
        """pass the message to each of the subscribers"""
        for k, v in self.subscribers.items():
            if(v.get_type() == message.type):
                notifier = Notifier(v, message)
                notifier.start()

if __name__ == "__main__":
    broker = Broker()
    first = LazySubscriber("sports")
    second = SimpleSubscriber("sports")
    third = SimpleSubscriber("science")
    fid = broker.register(first)
    broker.register(second)
    broker.register(third)
    broker.notify(Message("sports", "Mithali retires"))
    broker.notify(Message("science", "Deep Learning is almost like human"))
    broker.unregister(fid)
    broker.notify(Message("sports", "Rahul injured"))