from service.library import Library
from domain.models import Book
from errors.errors import LibraryError, BookNotFoundError, InvalidBookError

lib = Library()
book = Book(123, "Python", "Krishna")
lib.add(book)
print(lib.search_by(lambda b: b._title.startswith("Py")))