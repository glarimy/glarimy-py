class Book():
    def __init__(self, isbn, title, author):
        self._isbn = isbn
        self._title = title 
        self._author = author
    
    def print(self):
        print("ISBN:", self._isbn, ", Title: ", self._title, ", Author: ", self._author)