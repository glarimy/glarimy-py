def loggable(target):
    def log(*args, **kwargs):
        print("invoking the operation: ", target)
        results = target(*args, **kwargs)
        print("returning from operation: ", target)
        return results
    return log