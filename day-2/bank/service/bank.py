from utils.generator import get_id_generator
from utils.logger import loggable
from abc import ABC, abstractmethod

class Customer():
    def __init__(self, name:str, phone:int):
        self._name = name
        self._phone = phone

    def get_name(self):
        return self._name

class Account():
    def __init__(self, acc_no:str, customer: Customer):
        self._acc_no = acc_no
        self._customer = customer 
        self._balance = 0
    
    def get_balance(self):
        return self._balance
    
    def get_customer(self):
        return self._customer
    
    def get_acc_no(self):
        return self._acc_no
    
    def print(self):
        print("Number: ", self._acc_no, "Balance: ", self._balance, "Customer: ", self._customer._name)

    def deposit(self, amount:int): 
        if (amount % 100 == 0 and amount < 1000):
            self._balance += amount
            return self._balance
        raise ValueError

    def widthdraw(self, amount:int):
        if(self._balance < amount):
            raise ValueError
        self._balance -= amount
        return self._balance


class IBank(ABC):
    """ write the contract """
    @abstractmethod
    def open_account_for(self, customer:Customer): pass

    """ write the contract """
    @abstractmethod
    def deposit(self, amount:int, acc_no:str): pass

    """ write the contract """
    @abstractmethod
    def withdraw(self, amount:int, acc_no:str): pass

    """ write the contract """
    @abstractmethod
    def fetch_accounts_on(self, acc_no:str): pass

class Bank(IBank):
    def __init__(self):
        self._accounts = {}
        self._generator = get_id_generator()

    @loggable
    def open_account_for(self, customer:Customer):
        acc_no = next(self._generator)
        account = Account(acc_no, customer)
        self._accounts[acc_no] = account
        return acc_no

    @loggable
    def deposit(self, amount:int, acc_no:str):
        account = self._accounts[acc_no]
        return account.deposit(amount)

    @loggable
    def withdraw(self, amount:int, acc_no:str):
        account = self._accounts[acc_no]
        return account.widthdraw(amount)

        
    @loggable
    def fetch_accounts_on(self, criteria):
        return [account for acc_no, account in self._accounts.items() if criteria(account)]