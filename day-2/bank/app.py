from service.bank import IBank, Customer, Bank, Account

bank: IBank = Bank()

acc_no: str = bank.open_account_for(Customer("Krishna Mohan Koyya",9731423166))

bank.deposit(200, acc_no)
bank.withdraw(100, acc_no)
accounts: [Account] = bank.fetch_accounts_on(lambda account: account.get_balance() > 0)
for account in accounts:
    account.print()