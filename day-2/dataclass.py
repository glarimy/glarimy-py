from dataclasses import dataclass

@dataclass(frozen=TRUE)
class Person:
    name:str
    phone:int

me = Person("Krishna", 1234)
me.phone = 2345
print(me.name)