class Sorter:
    def __init__(self):
        pass 

    def _bubble(self, data):
        print('bubble sort')

    def _quick(self, data):
        print('quick sort')
    
    def __call__(self, data):
        if(len(data) > 5):
            return self._quick(data)
        else:
            return self._bubble(data)

sort = Sorter()
###############
from functor import sort

sort([1, 2, 3, 4, 5, 6])