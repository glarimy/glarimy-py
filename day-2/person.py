class Person():
    __slots__ = ("name", "phone", "address")
    def __init__(self, name, phone):
        self.name = name
        self.phone = phone

krishna = Person("Krishna", 123)
krishna.name = "Krishna Mohan"
mohan = Person("Mohan", 345)