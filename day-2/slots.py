class Repository():
    __slots__ = ('entries', 'name')
    def __init__(self):
        self.entries = []
    def add(self, entry):
        self.entries.append(entry)
    def list(self):
        return self.entries

class StableRepository(Repository):
    def add(self, entry):
        if(entry != None):
            super().add(entry)

repo1 = Repository()
repo1.name = "First Repo"
print(repo1.name)