class LibraryError(Exception):
    def __init__(self, message):
        self.message = message

class BookNotFoundError(LibraryError):
    def __init__(self, message):
        self.message = message

class InvalidBookError(LibraryError):
    def __init__(self, message):
        self.message = message