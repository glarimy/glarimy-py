from abc import ABC, abstractmethod
from errors.errors import LibraryError, InvalidBookError, BookNotFoundError
from domain.models import Book

class ILibrary(ABC):
    @abstractmethod
    def add(self, book):
        pass

    @abstractmethod
    def find_by(self, isbn):
        pass 
    
    @abstractmethod
    def search_by(self, condition):
        pass 

class Library(ILibrary):
    __slots__ = ["_books"]
    def __init__(self):
        self._books = {}
    
    def add(self, book):
        if(book == None):
            raise InvalidBookError()
        self._books[book._isbn] = book

    def find_by(self, isbn):
        if(self._books.contains_key(isbn)):
            return self._books[isbn]
        raise BookNotFoundError()
    
    def search_by(self, condition):
        return [isbn for isbn, book in self._books.items() if condition(book)]