class ISBN():
    def __init__(self, value):
        self._value = value
    def __get__(self, obj, type=None):
        return self._value
    def __set__(self, obj, value):
        if(value >1):
            self._value = value

class Book():
    __slots__ = ["_isbn", "_title", "_author"]
    def __init__(self, isbn, title, author):
        self._isbn = ISBN(isbn)
        self._title = title 
        self._author = author
    
    def print(self):
        print("ISBN:", self._isbn, ", Title: ", self._title, ", Author: ", self._author)