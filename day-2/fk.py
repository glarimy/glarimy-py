class Cache:
  def __new__(self):
    if not hasattr(self, 'instance'):
      self.instance = super().__new__(self)
    return self.instance

  def setValue(self, val):
    self.val = val

  def getVal(self):
    return self.val

s = Cache()
print("Object created:", s)
s.setValue(10)

s1 = Cache()
print("Object created:", s1)
print(s.getVal())