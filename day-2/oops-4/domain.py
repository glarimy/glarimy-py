class PhoneNumber:
    def __init__(self, value):
        if(value < 1):
            raise ValueError
        self._value = value

    @property
    def value(self):
        return self._value