from abc import ABC, abstractmethod

class Employee():
    def __init__(self, name, phone, email):
        self._name = name
        self._phone = phone
        self._email = email
    
    def set_id(self, id):
        self._id = id
    
    def print(self):
        print(self._id , "=>", self._name, "[", self._phone, "-", self._email, "]")

class IDirectory(ABC):
    """
    contract for the class
    """
    @abstractmethod
    def add(self, e):
        """
        contract for the method
        """
        pass

    @abstractmethod
    def find_by(self, id):
        """
        contract for the method
        """        
        pass


class SimpleDirectory(IDirectory):
    def __init__(self):
        self._employees = {}
        self._eid = 0

    def add(self, e):
        self._eid += 1
        e.set_id(self._eid)
        self._employees[self._eid] = e
        return self._eid

    def find_by(self, id):
        return self._employees[id]

dir = IDirectory()
krishna = Employee("Krishna", 9731423166, "krishna@glarimy.com")
eid = dir.add(krishna)
e = dir.find_by(eid)
e.print()