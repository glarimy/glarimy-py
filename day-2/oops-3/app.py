from dir.service import EmployeeDirectory
from dir.exceptions import EmployeeNotFoundError, InvalidEmployeeError, DirectoryError
from dir.domain import Employee

if __name__ == "__main__":
    directory = EmployeeDirectory()
    try:
        eid = directory.add(Employee("Krishna Mohan Koyya", 9831423166, "krishna@glarimy.com"))
        print("Added employee successfully")
        e = directory.find_by(eid)
        print("Retrived employee successfully")
        e.print()
    except InvalidEmployeeError as e:
        print(e)
    except EmployeeNotFoundError as e:
        print(e)
    except DirectoryError as e:
        print(e)