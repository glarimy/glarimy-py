from abc import ABC, abstractmethod

class IRepository(ABC):
    @abstractmethod
    def add(self, entity): pass
    
    @abstractmethod
    def list(self): pass    
    
    @abstractmethod
    def remove_by(self, id): pass
    
    @abstractmethod
    def find_by(self, id): pass

class InMemoryRepository(IRepository):
    def __init__(self):
        self._entries = {}

    def add(self, entity):
        self._entries[entity.get_id()] = entity
    
    def list(self):
        return self._entries.items()
    
    def remove_by(self, id):
        del self._entries[id]
    
    def find_by(self, id):
        return self._entries[id]

