from abc import ABC, abstractmethod

class IEntity(ABC):
    @abstractmethod
    def get_id(self): pass

class IPrintable(ABC):
    @abstractmethod
    def print(self): pass
    
class Employee(IEntity, IPrintable):
    def __init__(self, name, phone, email):
        self._id = None
        self._name = name
        self._phone = phone
        self._email = email 

    def get_id(self):
        return self._id

    def print(self):
        print('ID: ', self._id, ' Name: ', self._name)