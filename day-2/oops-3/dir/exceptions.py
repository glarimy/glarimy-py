class DirectoryError(Exception):
    def __init__(self, message):
        self.message = message

class EmployeeNotFoundError(DirectoryError):
    def __init__(self, message):
        super(message)

class InvalidEmployeeError(DirectoryError):
    def __init__(self, message):
        super(message)