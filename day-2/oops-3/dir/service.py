from dir.utils import id_generator
from dir.data import InMemoryRepository
from dir.exceptions import EmployeeNotFoundError, InvalidEmployeeError
from dir.domain import Employee
from abc import ABC, abstractmethod

class IDirectory(ABC):
    @abstractmethod
    def add(self, employee):
        """Adds the specified employee to the directory and returns the id"""

    @abstractmethod
    def remove_by(self, id):
        """Removes the employee with the specified id"""

    @abstractmethod
    def find_by(self, id):
        """Finds an employee by the specified id"""

    @abstractmethod
    def search_by(self, name):
        """Searches all employees with the specified name"""

    @abstractmethod
    def filter_by(self, condition): 
        """Searches all employees for the given condition"""

    @abstractmethod
    def count(self):
        """Returns the number of employees in the directory"""

    @abstractmethod
    def list(self):
        """Returns the complete list of employees"""

class EmployeeDirectory(IDirectory):
    def __init__(self):
        self._repo = InMemoryRepository()
        self._eid = id_generator()

    def add(self, employee):
        """Adds the specified employee to the directory and returns the id"""

        employee._id = next(self._eid)
        self._repo.add(employee)
        return employee.get_id()

    def remove_by(self, id):
        """Removes the employee with the specified id"""

        self._repo.remove_by(id)

    def find_by(self, id):
        """Finds an employee by the specified id"""

        emp = self._repo.find_by(id)
        if(emp == None):
            raise EmployeeNotFoundException(" not found")
        return emp

    def search_by(self, name):
        """Searches all employees with the specified name"""

        return [v for k, v in self._repo.list().items() if v["name"] == name]

    def filter_by(self, condition): 
        """Searches all employees for the given condition"""

        return [v for k, v in self._repo.list().items() if condition(v)]

    def count(self):
        """Returns the number of employees in the directory"""

        return len(self._repo.list())

    def list(self):
        """Returns the complete list of employees"""

        return self._repo.list()