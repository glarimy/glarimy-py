from service.bank import bank

open_account_for, deposit, withdraw, fetch_accounts_on = bank()

acc_no = open_account_for({
    "name": "Krishna Mohan Koyya",
    "phone": 9731423166
})

deposit(200, acc_no)
withdraw(100, acc_no)
accounts = fetch_accounts_on(lambda account: account["balance"] > 0)
print(accounts)