from utils.generator import get_id_generator
from utils.logger import loggable

def bank():
    accounts = {}
    generator = get_id_generator()

    @loggable
    def open_account_for(customer):
        acc_no = next(generator)
        account = {
            "acc_no": acc_no,
            "balance": 0,
            "customer": customer
        }
        accounts[acc_no] = account
        return acc_no

    @loggable
    def deposit(amount, acc_no):
        if (amount % 100 == 0 and amount < 1000):
            new_balance = accounts[acc_no]["balance"] + amount
            accounts[acc_no]["balance"] = new_balance
            return new_balance
        raise ValueError

    @loggable
    def withdraw(amount, acc_no):
        balance = accounts[acc_no]["balance"]
        if(balance < amount):
            raise ValueError
        new_balance = balance - amount
        accounts[acc_no]["balance"] = new_balance
        return new_balance
        
    @loggable
    def fetch_accounts_on(criteria):
        return [account for acc_no, account in accounts.items() if criteria(account)]

    return open_account_for, deposit, withdraw, fetch_accounts_on