def loggable(target):
    def capability(*args, **kwargs):
        print("logger: received ", args, " as parameters")
        result = target(*args, **kwargs)
        print("logger: returning ", result, "as result")
        return result
    return capability

def validatable(*rules):
    def decorate(target):
        def capability(*args, **kwargs):
            for index, arg in enumerate(args):
                if rules[index](arg) == False:
                    print("validator:", arg, " is invalid value for argument ", index+1)
                    return
            return target(*args, **kwargs)
        return capability
    return decorate

@loggable
@validatable(lambda a:a>0, lambda a:a>0, lambda a:a>0)
def interest(p, t, r):
    return p * t * r / 100

@loggable
def multiply(first, second):
    return first * second

#log_and_multiply = loggable(multiply)
#product = log_and_multiply(3, 5);
#print(multiply(3, 5))
#log_and_multiply(3,4)
print(multiply(3,4))
#product_of = loggable(validatable(lambda p:type(p)==int, lambda p:p>0)(multiply))

#print(interest(100, 3, 2))
#print(interest(100, 3, -2))
#print(product_of("ten", 5))
#print(product_of(10, 5))