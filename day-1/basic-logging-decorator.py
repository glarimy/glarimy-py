def multiply(first, second):
    return first * second

def sum(first, second):
    return first + second

def loggable(target):
    def capability(*args, **kwargs):
        print("logger: arguments => ", args) #pre-process
        result = target(*args) # delegate
        print("logger: result => ", result) #post-process
        return result

    return capability

print(multiply(10, 5))

product_of = loggable(multiply)
sum_of = loggable(sum)
print("product: ", product_of(10, 5))
print("sum: ", sum_of(10, 5))
