def get_id():
    """Generates a unique id starting from 1001, in sequence """

    id = 1000
    while id<1003:
        id += 1
        yield id
    
id_gen = get_id()
print(next(id_gen))
print(next(id_gen))
print(next(id_gen))
print(next(id_gen))
id_gen = get_id()
print(next(id_gen))
