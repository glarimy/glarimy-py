from service.directory import directory as directory

add, find_by = directory()
try:    
    id = add({"name": "Krishna", "email": "krishna@glarimy.com", "phone": 9731423166})
    print ("Found: " , find_by(1001))
    print (find_by(1002))
except ValueError: 
    print ("Invalid employee data")
except Exception:
    print ("Employee not found")