from utils.generator import get_id_generator

def directory():
    employees = {}
    id_generator = get_id_generator()

    def add(employee):
        """
        Adds the specified employee to the directory and returns the id
        
        Parameters: employee - a dictionary with valid name, email and phone
        Returns: eid - newly generated employee id
        Raises: ValueError - if employee is invalid
        """

        if(employee == None):
            raise ValueError

        eid = next(id_generator)
        employee["id"] = eid
        employees[eid] = employee
        return eid

    def find_by(id):
        """
        Finds an employee by the specified id
        
        Parameters: id - an integer
        Returns: employee - employee with the specified id, if found
        Raises: Exception - if employee with the specified id is not found
        """
        return employees[id]
    
    return add, find_by

