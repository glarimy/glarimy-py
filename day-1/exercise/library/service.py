def library():
    books = {}

    def add(book):
        """Adds the specified book to the system"""
        books[book["isbn"]] = book

    def find_by(isbn):
        """Finds a book by the specified isbn"""
        return books[isbn]

    def search_by(condition): 
        """Searches all books for the given condition and returns the ISBNs. The condition will be a lambda fx(book) => boolean)"""
        return [isbn for isbn, book in books.items() if condition(book)]

    return add, find_by, search_by