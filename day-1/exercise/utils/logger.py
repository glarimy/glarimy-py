import datetime

def loggable(target):
    def capability(*args, **kwargs):
        print(datetime.datetime.now(), ": invoking ", target, " with ",  args) 
        result = target(*args)
        print(datetime.datetime.now(), ": returning ", result)
        return result
    return capability