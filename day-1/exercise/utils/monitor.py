import time

def monitorable(target):
    def capability(*args, **kwargs):
        start = time.time_ns()
        result = target(*args)
        print(target, " took ", int(time.time_ns()-start))
        return result
    return capability