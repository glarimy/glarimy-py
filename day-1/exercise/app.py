from library.service import library
from utils.logger import loggable
from utils.monitor import monitorable

add, find_by, search_by = library()

add_and_log = loggable(add)
find_and_log = loggable(search_by)
search_and_log = loggable(search_by)
monitor = monitorable(search_and_log)

add_and_log({"isbn":1233, "title":"Python"})
add_and_log({"isbn":1234, "title":"Clean Code"})
add_and_log({"isbn":1235, "title":"Patterns"})

print(monitor(lambda b: b["isbn"] == 1233))